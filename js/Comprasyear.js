 var total=0;
    $('#get').click(function() {
        var datos=$('#form').serialize();
        jQuery.ajax({
            url: 'comprasMes.php',
            type: 'POST',
            dataType: 'json',
            data: datos,
            complete: function(xhr, textStatus) {
            },
            success: function(data, textStatus, xhr) {
                console.dir(data);
                graph(data,'container');
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log('Ocurrio Un error');
            }
        });

    });
     function ConvertFecha (f) {
        var D=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        var myarr = f.split("-");
        var i=parseInt(myarr[1])-1;
        return D[i];
    }
    //Obtener X
    function getId (json) {
        var a=[];
        for (var i = 0; i < json.length; i++) {
            a.push(ConvertFecha(json[i].Fecha));
        }
        return a;
    }
    //Obtener Y 
    function getData(json) {
        var b=[];
        total=0.0;
        var y=0.0;
        var iva=0.0;
        $("#tabla tr>td" ).remove();
        for (var i = 0; i < json.length; i++) {
            iva=parseFloat(json[i].Iva);
            if(isNaN(iva)){
                iva=0.0;
                json[i].Iva=0;
           }
            
            y=parseFloat(json[i].Costo)+iva;

            b.push(y);
            total+=y;
            $('#tabla tr:last').after('<tr><td>'+json[i].Piezas +'</td><td> $'+json[i].Costo+'</td><td>  $'+json[i].Iva+'</td><td>'+ConvertFecha(json[i].Fecha)+'</td></tr>');
        }
        $('#vTotal').text('$'+total);
        return b;
    };
    function graph (json,id) {
        $('#'+id).highcharts({
             chart: {
             type: 'column'
        },
        title: {
            text: 'Gráfica de Compras',
            x: -20 //center
        },
        subtitle: {
            text: 'Uptics Control',
            x: -20
        },
        xAxis: {
            categories: getId(json),
        },
        yAxis: {
            title: {
                text:'Total Vendido $'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>Información.<br/>Mes :{point.x}</b><br/>',
            pointFormat: '<span style="color:{series.color}"> Monto:${point.y}</spam>',
            shared: true,
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Información',
            data: getData(json),
        },
        ]
    });
}