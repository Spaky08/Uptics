 var total=0;
    $('#get').click(function() {
        var datos=$('#form').serialize();
        jQuery.ajax({
            url: 'compras.php',
            type: 'POST',
            dataType: 'json',
            data: datos,
            complete: function(xhr, textStatus) {
            },
            success: function(data, textStatus, xhr) {
                console.dir(data);
                graph(data,'container');
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log('Ocurrio Un error');
            }
        });

    });
    //Obtener X
    function getId (json) {
        var a=[];
        for (var i = 0; i < json.length; i++) {
            a.push(json[i].FechaCompra);
        }
        return a;
    }
    //Obtener Y 
    function getData(json) {
        var b=[];
        total=0.0;
        var y=0.0;
        $("#tabla tr>td" ).remove();
        for (var i = 0; i < json.length; i++) {
            y=parseFloat(json[i].Costo)
            b.push(y);
            total+=y;
            $('#tabla tr:last').after('<tr><td>'+json[i].nombreCom +'</td><td>'+json[i].TotalPiezas+'</td><td>$'+json[i].Costo+'</td><td>'+json[i].FechaCompra+'</td></tr>');
        }
        $('#vTotal').text('$'+total);
        return b;
    };
    function graph (json,id) {
        $('#'+id).highcharts({
        //     chart: {
        //     type: 'column'
        // },
        title: {
            text: 'Gráfica de Ventas',
            x: -20 //center
        },
        subtitle: {
            text: 'Uptics Control',
            x: -20
        },
        xAxis: {
            categories: getId(json),
        },
        yAxis: {
            title: {
                text:'Total Vendido $'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>Información.<br/>Dia :{point.x}</b><br/>',
            pointFormat: '<span style="color:{series.color}"> Monto:${point.y}</spam>',
            shared: true,
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Información',
            data: getData(json),
        },
        ]
    });
}