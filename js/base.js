$(document).ready(function() {

    $(".alerta .close").click(function(event) {
        $(this).parents(".alerta").hide();
        // $("#busqueda").val("");
        $(".virtualkeyboard").remove();
    });

    $(".pagina").css('height', window.innerHeight);
    var h = parseFloat($(".pagina").css('height'));
    $(".pagina .contenedor .body").css('height', (h-320)+"px");
    $(".listado").css('height', (h-430)+"px");

});

$(document).on('keydown', function(e) {
    if(e.ctrlKey && (e.which == 74)) {
        e.preventDefault();
        return false;
    }
});

function alerta(titulo, contenido){
    $("#alerta").hide();
    $("#alerta .header").html(titulo);
    $("#alerta .body").html(contenido);
    $("#alerta").show();
}

function closeTooltip(){
    $(".tooltip, .hide").hide();
}