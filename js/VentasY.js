 var total=0;
 $('#get').click(function() {
    var datos=$('#form').serialize();
    jQuery.ajax({
        url: 'VentasY.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
        complete: function(xhr, textStatus) {
        },
        success: function(data, textStatus, xhr) {
            graph(data,'container');
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log('Ocurrio Un error');
        }
    });
});
    //Obtener X
    function getId (json) {
        var D=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        return D;
    }
    //Obtener Datos
    function getData(json) {
        var b=[];
        total=0.0;
        var y=0.0;
        var R=[];
        var T=[];
        for (var i = 0; i < json.length; i++) {
            if(i==json.length-1){
                var A=json[i].spline;

                var F={
                    type: 'spline',
                    name: 'Promedios',
                    data: A,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                    }
                }
                console.log(F);
                R.push(F);
                continue;
            }
            var A=json[i].name;
            var f=json[i].data;
            var C= f.split(",");
            var V=[];
            for(var j=0;j<C.length;j++){

                V.push(parseFloat(C[j]));
            }
            console.log(V);
            var Obj={
                name:A,
                data:V,
            };
            R.push(Obj);
            total=0.0;
            Obj.length=0;
        }


        return R;
    };
    function graph (json,id) {
        $('#'+id).highcharts({
           chart: {
               type: 'column'
           },
           title: {
            text: 'Gráfica de Ventas',
            x: -20 //c<enter
        },
        subtitle: {
            text: 'Uptics Control',
            x: -20
        },
        xAxis: {
            categories: getId(json),
            title: {
                text: 'Meses',
                style: {
                    color: "#4A8EE8"
                }
            },
        },
        yAxis: {
            
            labels: {
                format: '${value}',
                style: {
                  //  color: Highcharts.getOptions().colors[2]
              }
          },
          title: {
            text: 'Total Vendido $',
            style: {
                color: "#E8560C"
            }
        },

        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:11px">{series.name}: </td>' +
        '<td style="padding:0;font-size:11px"><b>${point.y:.2f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
    },
    series:getData(json)
});
}