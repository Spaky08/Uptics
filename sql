-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:8889
-- Tiempo de generación: 06-03-2015 a las 02:14:29
-- Versión del servidor: 5.5.38
-- Versión de PHP: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `october`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_access_log`
--

CREATE TABLE `backend_access_log` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2015-03-06 04:35:17', '2015-03-06 04:35:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_users`
--

CREATE TABLE `backend_users` (
`id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `activated_at`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Manuel de Jesus', 'Toala Perez', 'admin', 'toa.l@hotmail.com', '$2y$10$yi7eJB3myMOFIXxcszSRT.7oicIYUwZgeJBiVRtcunusy505kz7Eu', NULL, '$2y$10$Wvh38n4GphP9noiADtnSH.G2HqyDxwZLCQ5lif/TitBPmdrZxp0Ye', NULL, '{"superuser":1}', 1, NULL, '2015-03-06 04:35:17', '2015-03-06 04:33:11', '2015-03-06 04:35:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Admins', NULL, '2015-03-06 04:33:11', '2015-03-06 04:33:11', 'admins', 'Default group for administrators', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_user_preferences`
--

INSERT INTO `backend_user_preferences` (`id`, `user_id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 1, 'backend', 'reportwidgets', 'dashboard', '{"report_container_dashboard_1":{"class":"System\\\\ReportWidgets\\\\Status","configuration":{"title":"backend::lang.dashboard.status.widget_title_default","ocWidgetWidth":"10"},"sortOrder":1}}'),
(2, 1, 'backend', 'backend', 'preferences', '{"locale":"es","user_id":"1"}'),
(3, 1, 'backend', 'editor', 'preferences', '{"font_size":"11","word_wrap":"fluid","code_folding":"manual","tab_size":"4","theme":"terminal","show_invisibles":"1","highlight_active_line":"1","use_hard_tabs":"0","show_gutter":"1","auto_closing":"1","user_id":"1"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
`id` int(10) unsigned NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cms_theme_data`
--

INSERT INTO `cms_theme_data` (`id`, `theme`, `data`, `created_at`, `updated_at`) VALUES
(1, 'multi-theme-master', '{"created_at":"2015-03-05 23:32:45","updated_at":"2015-03-05 23:32:45","site_name":"Ecorecikla"}', '2015-03-06 05:32:45', '2015-03-06 05:32:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
`id` int(10) unsigned NOT NULL,
  `master_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `deferred_bindings`
--

INSERT INTO `deferred_bindings` (`id`, `master_type`, `master_field`, `slave_type`, `slave_id`, `session_key`, `is_bind`, `created_at`, `updated_at`) VALUES
(2, 'RainLab\\Blog\\Models\\Post', 'featured_images', 'System\\Models\\File', '2', 'CJGw6DK2IVn0SzmOIrCyoysU0NdJgEJVx2day5Z0', 1, '2015-03-06 05:52:13', '2015-03-06 05:52:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs`
--

CREATE TABLE `jobs` (
`id` bigint(20) unsigned NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_10_01_000001_Db_Deferred_Bindings', 1),
('2013_10_01_000002_Db_System_Files', 1),
('2013_10_01_000003_Db_System_Plugin_Versions', 1),
('2013_10_01_000004_Db_System_Plugin_History', 1),
('2013_10_01_000005_Db_System_Settings', 1),
('2013_10_01_000006_Db_System_Parameters', 1),
('2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
('2013_10_01_000008_Db_System_Mail_Templates', 1),
('2013_10_01_000009_Db_System_Mail_Layouts', 1),
('2014_10_01_000010_Db_Jobs', 1),
('2014_10_01_000011_Db_System_Event_Logs', 1),
('2014_10_01_000012_Db_System_Request_Logs', 1),
('2014_10_01_000013_Db_System_Sessions', 1),
('2013_10_01_000001_Db_Backend_Users', 2),
('2013_10_01_000002_Db_Backend_User_Groups', 2),
('2013_10_01_000003_Db_Backend_Users_Groups', 2),
('2013_10_01_000004_Db_Backend_User_Throttle', 2),
('2014_01_04_000005_Db_Backend_User_Preferences', 2),
('2014_10_01_000006_Db_Backend_Access_Log', 2),
('2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
('2014_10_01_000001_Db_Cms_Theme_Data', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_blog_categories`
--

CREATE TABLE `rainlab_blog_categories` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rainlab_blog_categories`
--

INSERT INTO `rainlab_blog_categories` (`id`, `name`, `slug`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorized', 'uncategorized', NULL, NULL, '2015-03-06 04:33:10', '2015-03-06 04:33:10'),
(2, 'Basura Electronica', 'basura-electronica', NULL, NULL, '2015-03-06 05:45:20', '2015-03-06 05:45:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_blog_posts`
--

CREATE TABLE `rainlab_blog_posts` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `content_html` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rainlab_blog_posts`
--

INSERT INTO `rainlab_blog_posts` (`id`, `user_id`, `title`, `slug`, `excerpt`, `content`, `published_at`, `published`, `created_at`, `updated_at`, `content_html`) VALUES
(1, 1, 'Basura  Electronica', 'basura-electronica', 'Basura \r\n¿Qué es la basura electrónica? \r\nHoy nuestro planeta está en crisis ecológica por  diversas razones y una de ellas está ligada a la tecnología, la basura electrónica, compuesta de la chatarra de aparatos eléctricos o electrónicos que caen en desuso como celulares, computadores, videojuegos o electrodomésticos.', '<h2>¿Qué es la basura electrónica? </h2>\r\nHoy nuestro planeta está en crisis ecológica por  diversas razones y una de ellas está ligada a la tecnología, la basura electrónica, compuesta de la chatarra de aparatos eléctricos o electrónicos que caen en desuso como celulares, computadores, videojuegos o electrodomésticos.\r\nSe cree que la basura electrónica es el desecho que más aumenta en el mundo desarrollado, debido a la reducción del costo de reemplazar computadores, teléfonos móviles y otros aparatos electrónicos, y a la velocidad con que la tecnología se vuelve obsoleta.\r\nColocar este tipo de residuos en la basura, o dejarlos en manos de cartoneros, es poner en riesgo la salud de las personas y del ambiente, debido a que contienen componentes peligrosos.\r\n \r\nSITUACION EN EL MUNDO\r\nSegún el Programa de Naciones Unidas para el Medio Ambiente (PNUMA), en todo el planeta se producen cada año entre 20 y 50 millones de toneladas de basura electrónica.\r\nEsos residuos contienen más de 700 elementos, como plomo, cadmio y litio. La mitad de ellos son nocivos para la salud y el medio ambiente.\r\nLas denuncias indican que la mayor parte de los desechos proceden de los países desarrollados. China genera cada vez más cantidad de basura electrónica debido al aumento del nivel de vida de su población.\r\nCada mes se calcula que llega al continente africano el equivalente a 100.000 ordenadores usados\r\nEspaña produce más de 200.000 toneladas de basura tecnológica al año.\r\nHasta 80 por ciento de los desechos electrónicos que se generan en América Latina terminan en basureros.\r\nPakistán, China, India, Ghana y Nigeria son de los países subdesarrollados más afectados por el creciente problema de la basura electrónica\r\n \r\nSITUACION EN MEXICO\r\nMéxico es el segundo mercado latinoamericano en tecnología de información, después de Brasil.\r\nCada año se desechan en México entre 150 mil y 180 mil toneladas de basura electrónica, que incluye televisores, computadoras, teléfonos fijos y celulares, grabadoras y aparatos de sonido. Lo más grave es que estos desechos contienen sustancias peligrosas y no se manejan adecuadamente.\r\nLa basura electrónica empieza a inundar los tiraderos mexicanos. Este nuevo tipo de desperdicio se desarrolla con rapidez en México, generado por el alto crecimiento de la industria electrónica, principalmente computadoras y celulares-, se estima que para el año 2010 el número de mexicanos con computadoras llegará entre 40 y 50 millones.\r\n \r\nPROBLEMA AMBIENTALES\r\nLos aparatos electrónicos, en especial las computadoras y los celulares, son una mezcla explosiva para la sociedad: contienen metales pesados como plomo, mercurio, cadmio y berilio; químicos peligrosos como retardantes de fuego bromados, polibromobifenilos y tetrabromobisfenol. Además, usan con frecuencia polímeros con base de cloruro de vinilo.\r\n \r\n \r\n \r\nUn celular contiene entre 500 y mil componentes de los anteriormente descritos. Para tener una idea de la magnitud de la basura electrónica y sus consecuencias fatales, en 2004 se vendieron 674 millones de celulares en el mundo.\r\nLos componentes tóxicos más comunes son el plomo, el mercurio, y el cadmio. También llevan selenio y arsénico. Al ser fundidos liberan toxinas al aire, tierra y agua.\r\nLa liberación de sustancias tóxicas en rellenos sanitarios contamina el suelo circundante, pudiendo llegar a afectar fuentes de agua subterráneas. Además estas sustancias pueden disminuir notablemente la calidad del aire.\r\n \r\nSOLUCIONES\r\n \r\nNo tirar ni guardarla BASURA ELECTRONICA.\r\nNo entregar tu basura electrónica a empresas o personas no certificadas.\r\nAl entregar tu BASURA ELECTRONICA, solicita las licencias y permisos ambientales.\r\nSi alguna empresa de reciclaje electrónico no acopia monitores, televisiones, impresoras de tinta, esta creando un gran problema ambiental al dejarte lo más contaminante de la BASURA ELECTRONICA.', '2015-03-04 06:00:00', 1, '2015-03-06 05:42:47', '2015-03-06 05:52:32', '<h2>¿Qué es la basura electrónica? </h2>\n<p>Hoy nuestro planeta está en crisis ecológica por  diversas razones y una de ellas está ligada a la tecnología, la basura electrónica, compuesta de la chatarra de aparatos eléctricos o electrónicos que caen en desuso como celulares, computadores, videojuegos o electrodomésticos.\nSe cree que la basura electrónica es el desecho que más aumenta en el mundo desarrollado, debido a la reducción del costo de reemplazar computadores, teléfonos móviles y otros aparatos electrónicos, y a la velocidad con que la tecnología se vuelve obsoleta.\nColocar este tipo de residuos en la basura, o dejarlos en manos de cartoneros, es poner en riesgo la salud de las personas y del ambiente, debido a que contienen componentes peligrosos.</p>\n<p>SITUACION EN EL MUNDO\nSegún el Programa de Naciones Unidas para el Medio Ambiente (PNUMA), en todo el planeta se producen cada año entre 20 y 50 millones de toneladas de basura electrónica.\nEsos residuos contienen más de 700 elementos, como plomo, cadmio y litio. La mitad de ellos son nocivos para la salud y el medio ambiente.\nLas denuncias indican que la mayor parte de los desechos proceden de los países desarrollados. China genera cada vez más cantidad de basura electrónica debido al aumento del nivel de vida de su población.\nCada mes se calcula que llega al continente africano el equivalente a 100.000 ordenadores usados\nEspaña produce más de 200.000 toneladas de basura tecnológica al año.\nHasta 80 por ciento de los desechos electrónicos que se generan en América Latina terminan en basureros.\nPakistán, China, India, Ghana y Nigeria son de los países subdesarrollados más afectados por el creciente problema de la basura electrónica</p>\n<p>SITUACION EN MEXICO\nMéxico es el segundo mercado latinoamericano en tecnología de información, después de Brasil.\nCada año se desechan en México entre 150 mil y 180 mil toneladas de basura electrónica, que incluye televisores, computadoras, teléfonos fijos y celulares, grabadoras y aparatos de sonido. Lo más grave es que estos desechos contienen sustancias peligrosas y no se manejan adecuadamente.\nLa basura electrónica empieza a inundar los tiraderos mexicanos. Este nuevo tipo de desperdicio se desarrolla con rapidez en México, generado por el alto crecimiento de la industria electrónica, principalmente computadoras y celulares-, se estima que para el año 2010 el número de mexicanos con computadoras llegará entre 40 y 50 millones.</p>\n<p>PROBLEMA AMBIENTALES\nLos aparatos electrónicos, en especial las computadoras y los celulares, son una mezcla explosiva para la sociedad: contienen metales pesados como plomo, mercurio, cadmio y berilio; químicos peligrosos como retardantes de fuego bromados, polibromobifenilos y tetrabromobisfenol. Además, usan con frecuencia polímeros con base de cloruro de vinilo.</p>\n<p>Un celular contiene entre 500 y mil componentes de los anteriormente descritos. Para tener una idea de la magnitud de la basura electrónica y sus consecuencias fatales, en 2004 se vendieron 674 millones de celulares en el mundo.\nLos componentes tóxicos más comunes son el plomo, el mercurio, y el cadmio. También llevan selenio y arsénico. Al ser fundidos liberan toxinas al aire, tierra y agua.\nLa liberación de sustancias tóxicas en rellenos sanitarios contamina el suelo circundante, pudiendo llegar a afectar fuentes de agua subterráneas. Además estas sustancias pueden disminuir notablemente la calidad del aire.</p>\n<p>SOLUCIONES</p>\n<p>No tirar ni guardarla BASURA ELECTRONICA.\nNo entregar tu basura electrónica a empresas o personas no certificadas.\nAl entregar tu BASURA ELECTRONICA, solicita las licencias y permisos ambientales.\nSi alguna empresa de reciclaje electrónico no acopia monitores, televisiones, impresoras de tinta, esta creando un gran problema ambiental al dejarte lo más contaminante de la BASURA ELECTRONICA.</p>'),
(2, 1, 'Basura Electronica?', 'basura1-electronica', 'Se llama basura electrónica a todos aquellos dispositivos eléctricos o electrónicos que han llegado al final de su vida útil y, por lo tanto, son desechados. Computadoras viejas, celulares, electrodomésticos, reproductores de mp3, memorias USB, faxes, impresoras, etc.  Algunos se rompen y otros quedan obsoletos por el avance de la tecnología.', 'Se llama basura electrónica a todos aquellos dispositivos eléctricos o electrónicos que han llegado al final de su vida útil y, por lo tanto, son desechados. Computadoras viejas, celulares, electrodomésticos, reproductores de mp3, memorias USB, faxes, impresoras, etc.  Algunos se rompen y otros quedan obsoletos por el avance de la tecnología.\r\n\r\nEl problema que nos preocupa y sobre el cual estamos trabajando es que la basura electrónica es vertida a cielo abierto, lo cual resulta altamente contaminante. Los metales y demás elementos que poseen estos Residuos de Aparatos Eléctricos y Electrónicos (conocidos como RAEE) son tóxicos y contaminan el medio ambiente, perjudicando el aire que respiramos, la tierra y el agua que bebemos.', '2015-03-04 06:00:00', 1, '2015-03-06 05:50:27', '2015-03-06 05:52:13', '<p>Se llama basura electrónica a todos aquellos dispositivos eléctricos o electrónicos que han llegado al final de su vida útil y, por lo tanto, son desechados. Computadoras viejas, celulares, electrodomésticos, reproductores de mp3, memorias USB, faxes, impresoras, etc.  Algunos se rompen y otros quedan obsoletos por el avance de la tecnología.</p>\n<p>El problema que nos preocupa y sobre el cual estamos trabajando es que la basura electrónica es vertida a cielo abierto, lo cual resulta altamente contaminante. Los metales y demás elementos que poseen estos Residuos de Aparatos Eléctricos y Electrónicos (conocidos como RAEE) son tóxicos y contaminan el medio ambiente, perjudicando el aire que respiramos, la tierra y el agua que bebemos.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_blog_posts_categories`
--

CREATE TABLE `rainlab_blog_posts_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rainlab_blog_posts_categories`
--

INSERT INTO `rainlab_blog_posts_categories` (`post_id`, `category_id`) VALUES
(1, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_user_countries`
--

CREATE TABLE `rainlab_user_countries` (
`id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rainlab_user_countries`
--

INSERT INTO `rainlab_user_countries` (`id`, `is_enabled`, `name`, `code`) VALUES
(1, 1, 'United States', 'US'),
(2, 1, 'Canada', 'CA'),
(3, 1, 'Australia', 'AU'),
(4, 0, 'France', 'FR'),
(5, 0, 'Germany', 'DE'),
(6, 0, 'Iceland', 'IS'),
(7, 0, 'Ireland', 'IE'),
(8, 0, 'Italy', 'IT'),
(9, 0, 'Spain', 'ES'),
(10, 0, 'Sweden', 'SE'),
(11, 0, 'Austria', 'AT'),
(12, 0, 'Belgium', 'BE'),
(13, 0, 'Finland', 'FI'),
(14, 0, 'Czech Republic', 'CZ'),
(15, 0, 'Denmark', 'DK'),
(16, 0, 'Norway', 'NO'),
(17, 0, 'United Kingdom', 'GB'),
(18, 0, 'Switzerland', 'CH'),
(19, 0, 'New Zealand', 'NZ'),
(20, 0, 'Russian Federation', 'RU'),
(21, 0, 'Portugal', 'PT'),
(22, 0, 'Netherlands', 'NL'),
(23, 0, 'Isle of Man', 'IM'),
(24, 0, 'Afghanistan', 'AF'),
(25, 0, 'Aland Islands ', 'AX'),
(26, 0, 'Albania', 'AL'),
(27, 0, 'Algeria', 'DZ'),
(28, 0, 'American Samoa', 'AS'),
(29, 0, 'Andorra', 'AD'),
(30, 0, 'Angola', 'AO'),
(31, 0, 'Anguilla', 'AI'),
(32, 0, 'Antarctica', 'AQ'),
(33, 0, 'Antigua and Barbuda', 'AG'),
(34, 0, 'Argentina', 'AR'),
(35, 0, 'Armenia', 'AM'),
(36, 0, 'Aruba', 'AW'),
(37, 0, 'Azerbaijan', 'AZ'),
(38, 0, 'Bahamas', 'BS'),
(39, 0, 'Bahrain', 'BH'),
(40, 0, 'Bangladesh', 'BD'),
(41, 0, 'Barbados', 'BB'),
(42, 0, 'Belarus', 'BY'),
(43, 0, 'Belize', 'BZ'),
(44, 0, 'Benin', 'BJ'),
(45, 0, 'Bermuda', 'BM'),
(46, 0, 'Bhutan', 'BT'),
(47, 0, 'Bolivia, Plurinational State of', 'BO'),
(48, 0, 'Bonaire, Sint Eustatius and Saba', 'BQ'),
(49, 0, 'Bosnia and Herzegovina', 'BA'),
(50, 0, 'Botswana', 'BW'),
(51, 0, 'Bouvet Island', 'BV'),
(52, 0, 'Brazil', 'BR'),
(53, 0, 'British Indian Ocean Territory', 'IO'),
(54, 0, 'Brunei Darussalam', 'BN'),
(55, 0, 'Bulgaria', 'BG'),
(56, 0, 'Burkina Faso', 'BF'),
(57, 0, 'Burundi', 'BI'),
(58, 0, 'Cambodia', 'KH'),
(59, 0, 'Cameroon', 'CM'),
(60, 0, 'Cape Verde', 'CV'),
(61, 0, 'Cayman Islands', 'KY'),
(62, 0, 'Central African Republic', 'CF'),
(63, 0, 'Chad', 'TD'),
(64, 0, 'Chile', 'CL'),
(65, 0, 'China', 'CN'),
(66, 0, 'Christmas Island', 'CX'),
(67, 0, 'Cocos (Keeling) Islands', 'CC'),
(68, 0, 'Colombia', 'CO'),
(69, 0, 'Comoros', 'KM'),
(70, 0, 'Congo', 'CG'),
(71, 0, 'Congo, the Democratic Republic of the', 'CD'),
(72, 0, 'Cook Islands', 'CK'),
(73, 0, 'Costa Rica', 'CR'),
(74, 0, 'Cote d''Ivoire', 'CI'),
(75, 0, 'Croatia', 'HR'),
(76, 0, 'Cuba', 'CU'),
(77, 0, 'Curaçao', 'CW'),
(78, 0, 'Cyprus', 'CY'),
(79, 0, 'Djibouti', 'DJ'),
(80, 0, 'Dominica', 'DM'),
(81, 0, 'Dominican Republic', 'DO'),
(82, 0, 'Ecuador', 'EC'),
(83, 0, 'Egypt', 'EG'),
(84, 0, 'El Salvador', 'SV'),
(85, 0, 'Equatorial Guinea', 'GQ'),
(86, 0, 'Eritrea', 'ER'),
(87, 0, 'Estonia', 'EE'),
(88, 0, 'Ethiopia', 'ET'),
(89, 0, 'Falkland Islands (Malvinas)', 'FK'),
(90, 0, 'Faroe Islands', 'FO'),
(91, 0, 'Fiji', 'FJ'),
(92, 0, 'French Guiana', 'GF'),
(93, 0, 'French Polynesia', 'PF'),
(94, 0, 'French Southern Territories', 'TF'),
(95, 0, 'Gabon', 'GA'),
(96, 0, 'Gambia', 'GM'),
(97, 0, 'Georgia', 'GE'),
(98, 0, 'Ghana', 'GH'),
(99, 0, 'Gibraltar', 'GI'),
(100, 0, 'Greece', 'GR'),
(101, 0, 'Greenland', 'GL'),
(102, 0, 'Grenada', 'GD'),
(103, 0, 'Guadeloupe', 'GP'),
(104, 0, 'Guam', 'GU'),
(105, 0, 'Guatemala', 'GT'),
(106, 0, 'Guernsey', 'GG'),
(107, 0, 'Guinea', 'GN'),
(108, 0, 'Guinea-Bissau', 'GW'),
(109, 0, 'Guyana', 'GY'),
(110, 0, 'Haiti', 'HT'),
(111, 0, 'Heard Island and McDonald Islands', 'HM'),
(112, 0, 'Holy See (Vatican City State)', 'VA'),
(113, 0, 'Honduras', 'HN'),
(114, 0, 'Hong Kong', 'HK'),
(115, 0, 'Hungary', 'HU'),
(116, 0, 'India', 'IN'),
(117, 0, 'Indonesia', 'ID'),
(118, 0, 'Iran, Islamic Republic of', 'IR'),
(119, 0, 'Iraq', 'IQ'),
(120, 0, 'Israel', 'IL'),
(121, 0, 'Jamaica', 'JM'),
(122, 0, 'Japan', 'JP'),
(123, 0, 'Jersey', 'JE'),
(124, 0, 'Jordan', 'JO'),
(125, 0, 'Kazakhstan', 'KZ'),
(126, 0, 'Kenya', 'KE'),
(127, 0, 'Kiribati', 'KI'),
(128, 0, 'Korea, Democratic People''s Republic of', 'KP'),
(129, 0, 'Korea, Republic of', 'KR'),
(130, 0, 'Kuwait', 'KW'),
(131, 0, 'Kyrgyzstan', 'KG'),
(132, 0, 'Lao People''s Democratic Republic', 'LA'),
(133, 0, 'Latvia', 'LV'),
(134, 0, 'Lebanon', 'LB'),
(135, 0, 'Lesotho', 'LS'),
(136, 0, 'Liberia', 'LR'),
(137, 0, 'Libyan Arab Jamahiriya', 'LY'),
(138, 0, 'Liechtenstein', 'LI'),
(139, 0, 'Lithuania', 'LT'),
(140, 0, 'Luxembourg', 'LU'),
(141, 0, 'Macao', 'MO'),
(142, 0, 'Macedonia', 'MK'),
(143, 0, 'Madagascar', 'MG'),
(144, 0, 'Malawi', 'MW'),
(145, 0, 'Malaysia', 'MY'),
(146, 0, 'Maldives', 'MV'),
(147, 0, 'Mali', 'ML'),
(148, 0, 'Malta', 'MT'),
(149, 0, 'Marshall Islands', 'MH'),
(150, 0, 'Martinique', 'MQ'),
(151, 0, 'Mauritania', 'MR'),
(152, 0, 'Mauritius', 'MU'),
(153, 0, 'Mayotte', 'YT'),
(154, 0, 'Mexico', 'MX'),
(155, 0, 'Micronesia, Federated States of', 'FM'),
(156, 0, 'Moldova, Republic of', 'MD'),
(157, 0, 'Monaco', 'MC'),
(158, 0, 'Mongolia', 'MN'),
(159, 0, 'Montenegro', 'ME'),
(160, 0, 'Montserrat', 'MS'),
(161, 0, 'Morocco', 'MA'),
(162, 0, 'Mozambique', 'MZ'),
(163, 0, 'Myanmar', 'MM'),
(164, 0, 'Namibia', 'NA'),
(165, 0, 'Nauru', 'NR'),
(166, 0, 'Nepal', 'NP'),
(167, 0, 'New Caledonia', 'NC'),
(168, 0, 'Nicaragua', 'NI'),
(169, 0, 'Niger', 'NE'),
(170, 0, 'Nigeria', 'NG'),
(171, 0, 'Niue', 'NU'),
(172, 0, 'Norfolk Island', 'NF'),
(173, 0, 'Northern Mariana Islands', 'MP'),
(174, 0, 'Oman', 'OM'),
(175, 0, 'Pakistan', 'PK'),
(176, 0, 'Palau', 'PW'),
(177, 0, 'Palestinian Territory, Occupied', 'PS'),
(178, 0, 'Panama', 'PA'),
(179, 0, 'Papua New Guinea', 'PG'),
(180, 0, 'Paraguay', 'PY'),
(181, 0, 'Peru', 'PE'),
(182, 0, 'Philippines', 'PH'),
(183, 0, 'Pitcairn', 'PN'),
(184, 0, 'Poland', 'PL'),
(185, 0, 'Puerto Rico', 'PR'),
(186, 0, 'Qatar', 'QA'),
(187, 0, 'Reunion', 'RE'),
(188, 0, 'Romania', 'RO'),
(189, 0, 'Rwanda', 'RW'),
(190, 0, 'Saint Barthélemy', 'BL'),
(191, 0, 'Saint Helena', 'SH'),
(192, 0, 'Saint Kitts and Nevis', 'KN'),
(193, 0, 'Saint Lucia', 'LC'),
(194, 0, 'Saint Martin (French part)', 'MF'),
(195, 0, 'Saint Pierre and Miquelon', 'PM'),
(196, 0, 'Saint Vincent and the Grenadines', 'VC'),
(197, 0, 'Samoa', 'WS'),
(198, 0, 'San Marino', 'SM'),
(199, 0, 'Sao Tome and Principe', 'ST'),
(200, 0, 'Saudi Arabia', 'SA'),
(201, 0, 'Senegal', 'SN'),
(202, 0, 'Serbia', 'RS'),
(203, 0, 'Seychelles', 'SC'),
(204, 0, 'Sierra Leone', 'SL'),
(205, 0, 'Singapore', 'SG'),
(206, 0, 'Sint Maarten (Dutch part)', 'SX'),
(207, 0, 'Slovakia', 'SK'),
(208, 0, 'Slovenia', 'SI'),
(209, 0, 'Solomon Islands', 'SB'),
(210, 0, 'Somalia', 'SO'),
(211, 0, 'South Africa', 'ZA'),
(212, 0, 'South Georgia and the South Sandwich Islands', 'GS'),
(213, 0, 'Sri Lanka', 'LK'),
(214, 0, 'Sudan', 'SD'),
(215, 0, 'Suriname', 'SR'),
(216, 0, 'Svalbard and Jan Mayen', 'SJ'),
(217, 0, 'Swaziland', 'SZ'),
(218, 0, 'Syrian Arab Republic', 'SY'),
(219, 0, 'Taiwan, Province of China', 'TW'),
(220, 0, 'Tajikistan', 'TJ'),
(221, 0, 'Tanzania, United Republic of', 'TZ'),
(222, 0, 'Thailand', 'TH'),
(223, 0, 'Timor-Leste', 'TL'),
(224, 0, 'Togo', 'TG'),
(225, 0, 'Tokelau', 'TK'),
(226, 0, 'Tonga', 'TO'),
(227, 0, 'Trinidad and Tobago', 'TT'),
(228, 0, 'Tunisia', 'TN'),
(229, 0, 'Turkey', 'TR'),
(230, 0, 'Turkmenistan', 'TM'),
(231, 0, 'Turks and Caicos Islands', 'TC'),
(232, 0, 'Tuvalu', 'TV'),
(233, 0, 'Uganda', 'UG'),
(234, 0, 'Ukraine', 'UA'),
(235, 0, 'United Arab Emirates', 'AE'),
(236, 0, 'United States Minor Outlying Islands', 'UM'),
(237, 0, 'Uruguay', 'UY'),
(238, 0, 'Uzbekistan', 'UZ'),
(239, 0, 'Vanuatu', 'VU'),
(240, 0, 'Venezuela, Bolivarian Republic of', 'VE'),
(241, 0, 'Viet Nam', 'VN'),
(242, 0, 'Virgin Islands, British', 'VG'),
(243, 0, 'Virgin Islands, U.S.', 'VI'),
(244, 0, 'Wallis and Futuna', 'WF'),
(245, 0, 'Western Sahara', 'EH'),
(246, 0, 'Yemen', 'YE'),
(247, 0, 'Zambia', 'ZM'),
(248, 0, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_user_mail_blockers`
--

CREATE TABLE `rainlab_user_mail_blockers` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rainlab_user_states`
--

CREATE TABLE `rainlab_user_states` (
`id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rainlab_user_states`
--

INSERT INTO `rainlab_user_states` (`id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Alabama', 'AL'),
(2, 1, 'Alaska', 'AK'),
(3, 1, 'American Samoa', 'AS'),
(4, 1, 'Arizona', 'AZ'),
(5, 1, 'Arkansas', 'AR'),
(6, 1, 'California', 'CA'),
(7, 1, 'Colorado', 'CO'),
(8, 1, 'Connecticut', 'CT'),
(9, 1, 'Delaware', 'DE'),
(10, 1, 'Dist. of Columbia', 'DC'),
(11, 1, 'Florida', 'FL'),
(12, 1, 'Georgia', 'GA'),
(13, 1, 'Guam', 'GU'),
(14, 1, 'Hawaii', 'HI'),
(15, 1, 'Idaho', 'ID'),
(16, 1, 'Illinois', 'IL'),
(17, 1, 'Indiana', 'IN'),
(18, 1, 'Iowa', 'IA'),
(19, 1, 'Kansas', 'KS'),
(20, 1, 'Kentucky', 'KY'),
(21, 1, 'Louisiana', 'LA'),
(22, 1, 'Maine', 'ME'),
(23, 1, 'Maryland', 'MD'),
(24, 1, 'Marshall Islands', 'MH'),
(25, 1, 'Massachusetts', 'MA'),
(26, 1, 'Michigan', 'MI'),
(27, 1, 'Micronesia', 'FM'),
(28, 1, 'Minnesota', 'MN'),
(29, 1, 'Mississippi', 'MS'),
(30, 1, 'Missouri', 'MO'),
(31, 1, 'Montana', 'MT'),
(32, 1, 'Nebraska', 'NE'),
(33, 1, 'Nevada', 'NV'),
(34, 1, 'New Hampshire', 'NH'),
(35, 1, 'New Jersey', 'NJ'),
(36, 1, 'New Mexico', 'NM'),
(37, 1, 'New York', 'NY'),
(38, 1, 'North Carolina', 'NC'),
(39, 1, 'North Dakota', 'ND'),
(40, 1, 'Northern Marianas', 'MP'),
(41, 1, 'Ohio', 'OH'),
(42, 1, 'Oklahoma', 'OK'),
(43, 1, 'Oregon', 'OR'),
(44, 1, 'Palau', 'PW'),
(45, 1, 'Pennsylvania', 'PA'),
(46, 1, 'Puerto Rico', 'PR'),
(47, 1, 'Rhode Island', 'RI'),
(48, 1, 'South Carolina', 'SC'),
(49, 1, 'South Dakota', 'SD'),
(50, 1, 'Tennessee', 'TN'),
(51, 1, 'Texas', 'TX'),
(52, 1, 'Utah', 'UT'),
(53, 1, 'Vermont', 'VT'),
(54, 1, 'Virginia', 'VA'),
(55, 1, 'Virgin Islands', 'VI'),
(56, 1, 'Washington', 'WA'),
(57, 1, 'West Virginia', 'WV'),
(58, 1, 'Wisconsin', 'WI'),
(59, 1, 'Wyoming', 'WY'),
(60, 2, 'Alberta', 'AB'),
(61, 2, 'British Columbia', 'BC'),
(62, 2, 'Manitoba', 'MB'),
(63, 2, 'New Brunswick', 'NB'),
(64, 2, 'Newfoundland and Labrador', 'NL'),
(65, 2, 'Northwest Territories', 'NT'),
(66, 2, 'Nova Scotia', 'NS'),
(67, 2, 'Nunavut', 'NU'),
(68, 2, 'Ontario', 'ON'),
(69, 2, 'Prince Edward Island', 'PE'),
(70, 2, 'Quebec', 'QC'),
(71, 2, 'Saskatchewan', 'SK'),
(72, 2, 'Yukon', 'YT'),
(73, 3, 'New South Wales', 'NSW'),
(74, 3, 'Queensland', 'QLD'),
(75, 3, 'South Australia', 'SA'),
(76, 3, 'Tasmania', 'TAS'),
(77, 3, 'Victoria', 'VIC'),
(78, 3, 'Western Australia', 'WA'),
(79, 3, 'Northern Territory', 'NT'),
(80, 3, 'Australian Capital Territory', 'ACT'),
(81, 5, 'Baden-Württemberg', 'BW'),
(82, 5, 'Bavaria', 'BY'),
(83, 5, 'Berlin', 'BE'),
(84, 5, 'Brandenburg', 'BB'),
(85, 5, 'Bremen', 'HB'),
(86, 5, 'Hamburg', 'HH'),
(87, 5, 'Hesse', 'HE'),
(88, 5, 'Mecklenburg-Vorpommern', 'MV'),
(89, 5, 'Lower Saxony', 'NI'),
(90, 5, 'North Rhine-Westphalia', 'NW'),
(91, 5, 'Rhineland-Palatinate', 'RP'),
(92, 5, 'Saarland', 'SL'),
(93, 5, 'Saxony', 'SN'),
(94, 5, 'Saxony-Anhalt', 'ST'),
(95, 5, 'Schleswig-Holstein', 'SH'),
(96, 5, 'Thuringia', 'TH'),
(97, 7, 'Dublin', 'D'),
(98, 7, 'Wicklow', 'WW'),
(99, 7, 'Wexford', 'WX'),
(100, 7, 'Carlow', 'CW'),
(101, 7, 'Kildare', 'KE'),
(102, 7, 'Meath', 'MH'),
(103, 7, 'Louth', 'LH'),
(104, 7, 'Monaghan', 'MN'),
(105, 7, 'Cavan', 'CN'),
(106, 7, 'Longford', 'LD'),
(107, 7, 'Westmeath', 'WH'),
(108, 7, 'Offaly', 'OY'),
(109, 7, 'Laois', 'LS'),
(110, 7, 'Kilkenny', 'KK'),
(111, 7, 'Waterford', 'WD'),
(112, 7, 'Cork', 'C'),
(113, 7, 'Kerry', 'KY'),
(114, 7, 'Limerick', 'LK'),
(115, 7, 'North Tipperary', 'TN'),
(116, 7, 'South Tipperary', 'TS'),
(117, 7, 'Clare', 'CE'),
(118, 7, 'Galway', 'G'),
(119, 7, 'Mayo', 'MO'),
(120, 7, 'Roscommon', 'RN'),
(121, 7, 'Sligo', 'SO'),
(122, 7, 'Leitrim', 'LM'),
(123, 7, 'Donegal', 'DL'),
(124, 22, 'Drenthe', 'DR'),
(125, 22, 'Flevoland', 'FL'),
(126, 22, 'Friesland', 'FR'),
(127, 22, 'Gelderland', 'GE'),
(128, 22, 'Groningen', 'GR'),
(129, 22, 'Limburg', 'LI'),
(130, 22, 'Noord-Brabant', 'NB'),
(131, 22, 'Noord-Holland', 'NH'),
(132, 22, 'Overijssel', 'OV'),
(133, 22, 'Utrecht', 'UT'),
(134, 22, 'Zeeland', 'ZE'),
(135, 22, 'Zuid-Holland', 'ZH'),
(136, 17, 'Aberdeenshire', 'ABE'),
(137, 17, 'Anglesey', 'ALY'),
(138, 17, 'Angus', 'ANG'),
(139, 17, 'Argyll', 'ARG'),
(140, 17, 'Ayrshire', 'AYR'),
(141, 17, 'Banffshire', 'BAN'),
(142, 17, 'Bedfordshire', 'BED'),
(143, 17, 'Berkshire', 'BER'),
(144, 17, 'Berwickshire', 'BWS'),
(145, 17, 'Brecknockshire', 'BRE'),
(146, 17, 'Buckinghamshire', 'BUC'),
(147, 17, 'Bute', 'BUT'),
(148, 17, 'Caernarfonshire', 'CAE'),
(149, 17, 'Caithness', 'CAI'),
(150, 17, 'Cambridgeshire', 'CAM'),
(151, 17, 'Cardiganshire', 'CAR'),
(152, 17, 'Carmarthenshire', 'CMS'),
(153, 17, 'Cheshire', 'CHE'),
(154, 17, 'Clackmannanshire', 'CLA'),
(155, 17, 'Cleveland', 'CLE'),
(156, 17, 'Cornwall', 'COR'),
(157, 17, 'Cromartyshire', 'CRO'),
(158, 17, 'Cumberland', 'CBR'),
(159, 17, 'Cumbria', 'CUM'),
(160, 17, 'Denbighshire', 'DEN'),
(161, 17, 'Derbyshire', 'DER'),
(162, 17, 'Devon', 'DEV'),
(163, 17, 'Dorset', 'DOR'),
(164, 17, 'Dumbartonshire', 'DBS'),
(165, 17, 'Dumfriesshire', 'DUM'),
(166, 17, 'Durham', 'DUR'),
(167, 17, 'East Lothian', 'ELO'),
(168, 17, 'Essex', 'ESS'),
(169, 17, 'Flintshire', 'FLI'),
(170, 17, 'Fife', 'FIF'),
(171, 17, 'Glamorgan', 'GLA'),
(172, 17, 'Gloucestershire', 'GLO'),
(173, 17, 'Hampshire', 'HAM'),
(174, 17, 'Herefordshire', 'HER'),
(175, 17, 'Hertfordshire', 'HTF'),
(176, 17, 'Huntingdonshire', 'HUN'),
(177, 17, 'Inverness', 'INV'),
(178, 17, 'Kent', 'KEN'),
(179, 17, 'Kincardineshire', 'KCD'),
(180, 17, 'Kinross-shire', 'KIN'),
(181, 17, 'Kirkcudbrightshire', 'KIR'),
(182, 17, 'Lanarkshire', 'LKS'),
(183, 17, 'Lancashire', 'LAN'),
(184, 17, 'Leicestershire', 'LEI'),
(185, 17, 'Lincolnshire', 'LIN'),
(186, 17, 'London', 'LON'),
(187, 17, 'Manchester', 'MAN'),
(188, 17, 'Merionethshire', 'MER'),
(189, 17, 'Merseyside', 'MER'),
(190, 17, 'Middlesex', 'MDX'),
(191, 17, 'Midlands', 'MID'),
(192, 17, 'Midlothian', 'MLT'),
(193, 17, 'Monmouthshire', 'MON'),
(194, 17, 'Montgomeryshire', 'MGY'),
(195, 17, 'Moray', 'MOR'),
(196, 17, 'Nairnshire', 'NAI'),
(197, 17, 'Norfolk', 'NOR'),
(198, 17, 'Northamptonshire', 'NMP'),
(199, 17, 'Northumberland', 'NUM'),
(200, 17, 'Nottinghamshire', 'NOT'),
(201, 17, 'Orkney', 'ORK'),
(202, 17, 'Oxfordshire', 'OXF'),
(203, 17, 'Peebleshire', 'PEE'),
(204, 17, 'Pembrokeshire', 'PEM'),
(205, 17, 'Perthshire', 'PER'),
(206, 17, 'Radnorshire', 'RAD'),
(207, 17, 'Renfrewshire', 'REN'),
(208, 17, 'Ross & Cromarty', 'ROS'),
(209, 17, 'Roxburghshire', 'ROX'),
(210, 17, 'Rutland', 'RUT'),
(211, 17, 'Selkirkshire', 'SEL'),
(212, 17, 'Shetland', 'SHE'),
(213, 17, 'Shropshire', 'SHR'),
(214, 17, 'Somerset', 'SOM'),
(215, 17, 'Staffordshire', 'STA'),
(216, 17, 'Stirlingshire', 'STI'),
(217, 17, 'Suffolk', 'SUF'),
(218, 17, 'Surrey', 'SUR'),
(219, 17, 'Sussex', 'SUS'),
(220, 17, 'Sutherland', 'SUT'),
(221, 17, 'Tyne & Wear', 'TYN'),
(222, 17, 'Warwickshire', 'WAR'),
(223, 17, 'West Lothian', 'WLO'),
(224, 17, 'Westmorland', 'WES'),
(225, 17, 'Wigtownshire', 'WIG'),
(226, 17, 'Wiltshire', 'WIL'),
(227, 17, 'Worcestershire', 'WOR'),
(228, 17, 'Yorkshire', 'YOR'),
(229, 115, 'Budapest', 'BUD'),
(230, 115, 'Baranya', 'BAR'),
(231, 115, 'Bács-Kiskun', 'BKM'),
(232, 115, 'Békés', 'BEK'),
(233, 115, 'Borsod-Abaúj-Zemplén', 'BAZ'),
(234, 115, 'Csongrád', 'CSO'),
(235, 115, 'Fejér', 'FEJ'),
(236, 115, 'Győr-Moson-Sopron', 'GMS'),
(237, 115, 'Hajdú-Bihar', 'HBM'),
(238, 115, 'Heves', 'HEV'),
(239, 115, 'Jász-Nagykun-Szolnok', 'JNS'),
(240, 115, 'Komárom-Esztergom', 'KEM'),
(241, 115, 'Nógrád', 'NOG'),
(242, 115, 'Pest', 'PES'),
(243, 115, 'Somogy', 'SOM'),
(244, 115, 'Szabolcs-Szatmár-Bereg', 'SSB'),
(245, 115, 'Tolna', 'TOL'),
(246, 115, 'Vas', 'VAS'),
(247, 115, 'Veszprém', 'VES'),
(248, 115, 'Zala', 'ZAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci,
  `last_activity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_event_logs`
--

CREATE TABLE `system_event_logs` (
`id` int(10) unsigned NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `details` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'exception ''ErrorException'' with message ''Division by zero'' in /Applications/MAMP/htdocs/october/storage/cms/cache/14/80/ajax.htm.php:21\nStack trace:\n#0 /Applications/MAMP/htdocs/october/storage/cms/cache/14/80/ajax.htm.php(21): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, ''Division by zer...'', ''/Applications/M...'', 21, Array)\n#1 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(574): Cms54f8dac1f1c90_1128188647Class->onTest()\n#2 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(500): Cms\\Classes\\Controller->runAjaxHandler(''onTest'')\n#3 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(282): Cms\\Classes\\Controller->execAjaxHandlers()\n#4 [internal function]: Cms\\Classes\\Controller->run(''demo/ajax'')\n#5 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(1237): call_user_func_array(Array, Array)\n#6 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(162): Cms\\Classes\\Controller->callAction(''run'', Array)\n#7 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(107): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), ''run'')\n#8 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#9 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#10 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#12 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(108): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#13 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''run'')\n#14 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(198): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''Cms\\\\Classes\\\\Con...'', ''run'')\n#15 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(131): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#16 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(691): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#17 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#19 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#21 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(693): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#22 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#23 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(618): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#24 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(178): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#25 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#27 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(55): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(61): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(36): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(40): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(42): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(111): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#40 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(84): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/october/index.php(44): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#42 {main}', NULL, '2015-03-06 04:38:54', '2015-03-06 04:38:54'),
(2, 'error', 'exception ''ErrorException'' with message ''Division by zero'' in /Applications/MAMP/htdocs/october/storage/cms/twig/bf/c8/b5a7a31496b4a5b993163b528ba798de90c733f5d8b04514a0f59eabc2ed.php:193\nStack trace:\n#0 /Applications/MAMP/htdocs/october/storage/cms/twig/bf/c8/b5a7a31496b4a5b993163b528ba798de90c733f5d8b04514a0f59eabc2ed.php(193): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, ''Division by zer...'', ''/Applications/M...'', 193, Array)\n#1 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php(289): __TwigTemplate_bfc8b5a7a31496b4a5b993163b528ba798de90c733f5d8b04514a0f59eabc2ed->doDisplay(Array, Array)\n#2 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php(263): Twig_Template->displayWithErrorHandling(Array, Array)\n#3 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php(274): Twig_Template->display(Array)\n#4 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(330): Twig_Template->render(Array)\n#5 [internal function]: Cms\\Classes\\Controller->run(''/'')\n#6 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(1237): call_user_func_array(Array, Array)\n#7 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(162): Cms\\Classes\\Controller->callAction(''run'', Array)\n#8 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(107): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), ''run'')\n#9 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#10 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#11 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#13 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(108): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#14 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''run'')\n#15 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(198): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''Cms\\\\Classes\\\\Con...'', ''run'')\n#16 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(131): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#17 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(691): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#18 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#20 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(693): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#23 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#24 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(618): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(178): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#26 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(55): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(61): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(36): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(40): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(42): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(111): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(84): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /Applications/MAMP/htdocs/october/index.php(44): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}\n\nNext exception ''Twig_Error_Runtime'' with message ''An exception has been thrown during the rendering of a template ("Division by zero") in "/Applications/MAMP/htdocs/october/themes/multi-theme-master/layouts/default.htm" at line 91.'' in /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php:304\nStack trace:\n#0 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php(263): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Template.php(274): Twig_Template->display(Array)\n#2 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(330): Twig_Template->render(Array)\n#3 [internal function]: Cms\\Classes\\Controller->run(''/'')\n#4 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(1237): call_user_func_array(Array, Array)\n#5 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(162): Cms\\Classes\\Controller->callAction(''run'', Array)\n#6 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(107): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), ''run'')\n#7 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#8 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#9 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#10 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#11 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(108): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#12 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''run'')\n#13 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(198): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''Cms\\\\Classes\\\\Con...'', ''run'')\n#14 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(131): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#15 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(691): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#16 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#18 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#20 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(693): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#21 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(618): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#23 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(178): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#24 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(55): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(61): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(36): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(40): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(42): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#38 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(111): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#39 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(84): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/october/index.php(44): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#41 {main}', NULL, '2015-03-06 07:03:58', '2015-03-06 07:03:58'),
(3, 'error', 'exception ''Twig_Error_Syntax'' with message ''Unexpected character "''" in "/Applications/MAMP/htdocs/october/themes/multi-theme-master/layouts/default.htm" at line 106'' in /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Lexer.php:284\nStack trace:\n#0 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Lexer.php(216): Twig_Lexer->lexExpression()\n#1 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Lexer.php(115): Twig_Lexer->lexVar()\n#2 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Environment.php(505): Twig_Lexer->tokenize(''<!DOCTYPE html>...'', ''/Applications/M...'')\n#3 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Environment.php(595): Twig_Environment->tokenize(''<!DOCTYPE html>...'', ''/Applications/M...'')\n#4 /Applications/MAMP/htdocs/october/vendor/twig/twig/lib/Twig/Environment.php(335): Twig_Environment->compileSource(''<!DOCTYPE html>...'', ''/Applications/M...'')\n#5 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(329): Twig_Environment->loadTemplate(''/Applications/M...'')\n#6 [internal function]: Cms\\Classes\\Controller->run(''/'')\n#7 /Applications/MAMP/htdocs/october/modules/cms/Classes/Controller.php(1237): call_user_func_array(Array, Array)\n#8 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(162): Cms\\Classes\\Controller->callAction(''run'', Array)\n#9 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(107): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), ''run'')\n#10 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#12 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#14 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(108): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\Controller), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''run'')\n#16 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(198): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), ''Cms\\\\Classes\\\\Con...'', ''run'')\n#17 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Route.php(131): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#18 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(691): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#19 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#21 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#23 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(693): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#24 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Routing/Router.php(618): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(178): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#27 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(141): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(55): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#31 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(61): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(36): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(40): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#37 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(42): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(125): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(101): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(111): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /Applications/MAMP/htdocs/october/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(84): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /Applications/MAMP/htdocs/october/index.php(44): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}', NULL, '2015-03-06 07:06:21', '2015-03-06 07:06:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_files`
--

CREATE TABLE `system_files` (
`id` int(10) unsigned NOT NULL,
  `disk_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, '54f8e9e0342e6521949384.jpg', 'eco.jpg', 19012, 'image/jpeg', NULL, NULL, 'featured_images', '1', 'RainLab\\Blog\\Models\\Post', 1, 1, '2015-03-06 05:42:24', '2015-03-06 05:42:47'),
(2, '54f8ec2d93cb3203805422.jpg', 'eco2.jpg', 146271, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 2, '2015-03-06 05:52:13', '2015-03-06 05:52:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `content_css` text COLLATE utf8_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
(1, 'Default', 'default', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ message|raw }}\n    </body>\n</html>', '{{ message|raw }}', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2015-03-06 04:33:11', '2015-03-06 04:33:11'),
(2, 'System', 'system', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ message|raw }}\n        <hr />\n        <p>This is an automatic message. Please do not reply to it.</p>\n    </body>\n</html>', '{{ message|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2015-03-06 04:33:11', '2015-03-06 04:33:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_parameters`
--

CREATE TABLE `system_parameters` (
`id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'system', 'core', 'hash', '"7a25adc8a17cd662651b90126da4cea8"'),
(3, 'system', 'core', 'build', '"215"'),
(4, 'system', 'update', 'retry', '1425683522'),
(5, 'cms', 'theme', 'active', '"multi-theme-master"');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'RainLab.Blog', 'script', '1.0.1', 'create_posts_table.php', '2015-03-06 04:33:09'),
(2, 'RainLab.Blog', 'script', '1.0.1', 'create_categories_table.php', '2015-03-06 04:33:10'),
(3, 'RainLab.Blog', 'script', '1.0.1', 'seed_all_tables.php', '2015-03-06 04:33:10'),
(4, 'RainLab.Blog', 'comment', '1.0.1', 'Initialize plugin.', '2015-03-06 04:33:10'),
(5, 'RainLab.Blog', 'script', '1.0.2', 'create_posts_content_html.php', '2015-03-06 04:33:11'),
(6, 'RainLab.Blog', 'comment', '1.0.2', 'Added the processed HTML content column to the posts table.', '2015-03-06 04:33:11'),
(7, 'RainLab.Blog', 'comment', '1.0.3', 'Category component has been merged with Posts component.', '2015-03-06 04:33:11'),
(8, 'RainLab.Blog', 'comment', '1.0.4', 'Improvements to the Posts list management UI.', '2015-03-06 04:33:11'),
(9, 'RainLab.Blog', 'comment', '1.0.5', 'Removes the Author column from blog post list.', '2015-03-06 04:33:11'),
(10, 'RainLab.Blog', 'comment', '1.0.6', 'Featured images now appear in the Post component.', '2015-03-06 04:33:11'),
(11, 'RainLab.Blog', 'comment', '1.0.7', 'Added support for the Static Pages menus.', '2015-03-06 04:33:11'),
(12, 'RainLab.Blog', 'comment', '1.0.8', 'Added total posts to category list.', '2015-03-06 04:33:11'),
(13, 'RainLab.Blog', 'comment', '1.0.9', 'Added support for the Sitemap plugin.', '2015-03-06 04:33:11'),
(14, 'RainLab.Blog', 'comment', '1.0.10', 'Added permission to prevent users from seeing posts they did not create.', '2015-03-06 04:33:11'),
(15, 'RainLab.Blog', 'comment', '1.0.11', 'Deprecate "idParam" component property in favour of "slug" property.', '2015-03-06 04:33:11'),
(16, 'RainLab.Blog', 'comment', '1.0.12', 'Fixes issue where images cannot be uploaded caused by latest Markdown library.', '2015-03-06 04:33:11'),
(17, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2015-03-06 05:23:43'),
(18, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2015-03-06 05:23:44'),
(19, 'RainLab.User', 'script', '1.0.1', 'create_states_table.php', '2015-03-06 05:23:45'),
(20, 'RainLab.User', 'script', '1.0.1', 'create_countries_table.php', '2015-03-06 05:23:45'),
(21, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2015-03-06 05:23:45'),
(22, 'RainLab.User', 'script', '1.0.2', 'seed_all_tables.php', '2015-03-06 05:23:47'),
(23, 'RainLab.User', 'comment', '1.0.2', 'Add seed data for countries and states.', '2015-03-06 05:23:47'),
(24, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2015-03-06 05:23:47'),
(25, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2015-03-06 05:23:47'),
(26, 'RainLab.User', 'script', '1.0.5', 'users_add_contact_details.php', '2015-03-06 05:23:47'),
(27, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2015-03-06 05:23:47'),
(28, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2015-03-06 05:23:48'),
(29, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2015-03-06 05:23:48'),
(30, 'RainLab.User', 'comment', '1.0.7', 'Adds default country and state fields to Settings page.', '2015-03-06 05:23:48'),
(31, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2015-03-06 05:23:48'),
(32, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2015-03-06 05:23:48'),
(33, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2015-03-06 05:23:48'),
(34, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2015-03-06 05:23:49'),
(35, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2015-03-06 05:23:49'),
(36, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2015-03-06 05:23:50'),
(37, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2015-03-06 05:23:50'),
(38, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2015-03-06 05:23:50'),
(39, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2015-03-06 05:23:50'),
(40, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2015-03-06 05:23:50'),
(41, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2015-03-06 05:23:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`) VALUES
(1, 'RainLab.Blog', '1.0.12', '2015-03-06 04:33:11', 0),
(2, 'RainLab.User', '1.0.15', '2015-03-06 05:23:50', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_request_logs`
--

CREATE TABLE `system_request_logs` (
`id` int(10) unsigned NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `system_request_logs`
--

INSERT INTO `system_request_logs` (`id`, `status_code`, `url`, `referer`, `count`, `created_at`, `updated_at`) VALUES
(1, 404, 'http://localhost:8888/october/install.php', '["http:\\/\\/localhost:8888\\/october\\/"]', 1, '2015-03-06 05:31:14', '2015-03-06 05:31:14'),
(2, 404, 'http://localhost:8888/october/blog', NULL, 2, '2015-03-06 05:33:05', '2015-03-06 06:19:23'),
(3, 404, 'http://localhost:8888/october/blog/post', NULL, 1, '2015-03-06 06:16:20', '2015-03-06 06:16:20'),
(4, 404, 'http://localhost:8888/october/blog/post/1', NULL, 1, '2015-03-06 06:16:23', '2015-03-06 06:16:23'),
(5, 404, 'http://localhost:8888/october/assets/js/bootstrap.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:10', '2015-03-06 07:00:52'),
(6, 404, 'http://localhost:8888/october/assets/js/plugins/prettyphoto/jquery.prettyPhoto.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:10', '2015-03-06 07:00:52'),
(7, 404, 'http://localhost:8888/october/assets/js/jquery-1.11.1.min.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:10', '2015-03-06 07:00:52'),
(8, 404, 'http://localhost:8888/october/assets/js/jquery.bxslider.min.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:10', '2015-03-06 07:00:52'),
(9, 404, 'http://localhost:8888/october/assets/js/plugins/rainyday/rainyday.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:10', '2015-03-06 07:00:53'),
(10, 404, 'http://localhost:8888/october/assets/js/jquery.mixitup.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:11', '2015-03-06 07:00:53'),
(11, 404, 'http://localhost:8888/october/assets/js/circles.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:55'),
(12, 404, 'http://localhost:8888/october/assets/js/scrollReveal.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:54'),
(13, 404, 'http://localhost:8888/october/assets/js/plugins/countto/jquery.countTo.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:53'),
(14, 404, 'http://localhost:8888/october/assets/js/plugins/countto/jquery.appear.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:53'),
(15, 404, 'http://localhost:8888/october/assets/js/plugins/parallax/jquery.parallax-1.1.3.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:54'),
(16, 404, 'http://localhost:8888/october/assets/js/plugins/revolution/js/jquery.themepunch.revolution.min.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:12', '2015-03-06 07:00:54'),
(17, 404, 'http://localhost:8888/october/assets/js/plugins/smoothscroll/smoothScroll.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:13', '2015-03-06 07:00:55'),
(18, 404, 'http://localhost:8888/october/assets/js/custom.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:13', '2015-03-06 07:00:55'),
(19, 404, 'http://localhost:8888/october/assets/js/plugins/revolution/js/jquery.themepunch.tools.min.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:13', '2015-03-06 07:00:54'),
(20, 404, 'http://localhost:8888/october/assets/js/plugins/jquery-ui/jquery-ui.js', '["http:\\/\\/localhost:8888\\/october\\/"]', 5, '2015-03-06 06:53:13', '2015-03-06 07:00:55'),
(21, 404, 'http://localhost:8888/october/themes/multi-theme-master/assets/images/ico/favicon.ico', NULL, 1, '2015-03-06 07:00:55', '2015-03-06 07:00:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `system_settings`
--

CREATE TABLE `system_settings` (
`id` int(10) unsigned NOT NULL,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_throttle`
--

CREATE TABLE `user_throttle` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `backend_access_log`
--
ALTER TABLE `backend_access_log`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `backend_users`
--
ALTER TABLE `backend_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `backend_users_login_unique` (`login`), ADD UNIQUE KEY `backend_users_email_unique` (`email`), ADD KEY `backend_users_activation_code_index` (`activation_code`), ADD KEY `backend_users_reset_password_code_index` (`reset_password_code`);

--
-- Indices de la tabla `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
 ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indices de la tabla `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `backend_user_groups_name_unique` (`name`), ADD KEY `backend_user_groups_code_index` (`code`);

--
-- Indices de la tabla `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
 ADD PRIMARY KEY (`id`), ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indices de la tabla `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
 ADD PRIMARY KEY (`id`), ADD KEY `backend_user_throttle_user_id_index` (`user_id`), ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indices de la tabla `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
 ADD PRIMARY KEY (`id`), ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indices de la tabla `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
 ADD PRIMARY KEY (`id`), ADD KEY `deferred_bindings_master_type_index` (`master_type`), ADD KEY `deferred_bindings_master_field_index` (`master_field`), ADD KEY `deferred_bindings_slave_type_index` (`slave_type`), ADD KEY `deferred_bindings_slave_id_index` (`slave_id`);

--
-- Indices de la tabla `jobs`
--
ALTER TABLE `jobs`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rainlab_blog_categories`
--
ALTER TABLE `rainlab_blog_categories`
 ADD PRIMARY KEY (`id`), ADD KEY `rainlab_blog_categories_slug_index` (`slug`);

--
-- Indices de la tabla `rainlab_blog_posts`
--
ALTER TABLE `rainlab_blog_posts`
 ADD PRIMARY KEY (`id`), ADD KEY `rainlab_blog_posts_user_id_index` (`user_id`), ADD KEY `rainlab_blog_posts_slug_index` (`slug`);

--
-- Indices de la tabla `rainlab_blog_posts_categories`
--
ALTER TABLE `rainlab_blog_posts_categories`
 ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indices de la tabla `rainlab_user_countries`
--
ALTER TABLE `rainlab_user_countries`
 ADD PRIMARY KEY (`id`), ADD KEY `rainlab_user_countries_name_index` (`name`);

--
-- Indices de la tabla `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
 ADD PRIMARY KEY (`id`), ADD KEY `rainlab_user_mail_blockers_email_index` (`email`), ADD KEY `rainlab_user_mail_blockers_template_index` (`template`), ADD KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`);

--
-- Indices de la tabla `rainlab_user_states`
--
ALTER TABLE `rainlab_user_states`
 ADD PRIMARY KEY (`id`), ADD KEY `rainlab_user_states_country_id_index` (`country_id`), ADD KEY `rainlab_user_states_name_index` (`name`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
 ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indices de la tabla `system_event_logs`
--
ALTER TABLE `system_event_logs`
 ADD PRIMARY KEY (`id`), ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indices de la tabla `system_files`
--
ALTER TABLE `system_files`
 ADD PRIMARY KEY (`id`), ADD KEY `system_files_field_index` (`field`), ADD KEY `system_files_attachment_id_index` (`attachment_id`), ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indices de la tabla `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
 ADD PRIMARY KEY (`id`), ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indices de la tabla `system_parameters`
--
ALTER TABLE `system_parameters`
 ADD PRIMARY KEY (`id`), ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indices de la tabla `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
 ADD PRIMARY KEY (`id`), ADD KEY `system_plugin_history_code_index` (`code`), ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indices de la tabla `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
 ADD PRIMARY KEY (`id`), ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indices de la tabla `system_request_logs`
--
ALTER TABLE `system_request_logs`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `system_settings`
--
ALTER TABLE `system_settings`
 ADD PRIMARY KEY (`id`), ADD KEY `system_settings_item_index` (`item`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD UNIQUE KEY `users_login_unique` (`username`), ADD KEY `users_activation_code_index` (`activation_code`), ADD KEY `users_reset_password_code_index` (`reset_password_code`), ADD KEY `users_country_id_index` (`country_id`), ADD KEY `users_state_id_index` (`state_id`), ADD KEY `users_login_index` (`username`);

--
-- Indices de la tabla `user_throttle`
--
ALTER TABLE `user_throttle`
 ADD PRIMARY KEY (`id`), ADD KEY `user_throttle_user_id_index` (`user_id`), ADD KEY `user_throttle_ip_address_index` (`ip_address`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `backend_access_log`
--
ALTER TABLE `backend_access_log`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `backend_users`
--
ALTER TABLE `backend_users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `jobs`
--
ALTER TABLE `jobs`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rainlab_blog_categories`
--
ALTER TABLE `rainlab_blog_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `rainlab_blog_posts`
--
ALTER TABLE `rainlab_blog_posts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `rainlab_user_countries`
--
ALTER TABLE `rainlab_user_countries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT de la tabla `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rainlab_user_states`
--
ALTER TABLE `rainlab_user_states`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT de la tabla `system_event_logs`
--
ALTER TABLE `system_event_logs`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `system_files`
--
ALTER TABLE `system_files`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `system_parameters`
--
ALTER TABLE `system_parameters`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `system_request_logs`
--
ALTER TABLE `system_request_logs`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `system_settings`
--
ALTER TABLE `system_settings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_throttle`
--
ALTER TABLE `user_throttle`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;