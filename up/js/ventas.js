 var total=0;
 $('#get').click(function() {
    Process();
});
 $(document).ready(function(){
    Process();
 });
 function Process () {
    var datos=$('#form').serialize();
    jQuery.ajax({
        url: 'ventas.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
        complete: function(xhr, textStatus) {
        },
        success: function(data, textStatus, xhr) {

            graph(data,'container');
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log('Ocurrio Un error');
        }
    });
}
    //Obtener X
    function getId (json) {
        var a=[];
        for (var i = 0; i < json.length; i++) {
            a.push(json[i].FechaCorte);
        }
        return a;
    }
    //Obtener Y 
    function getData(json) {
        var b=[];
        total=0.0;
        var y=0.0;
        $("#tabla tr>td" ).remove();
        for (var i = 0; i < json.length; i++) {
            y=parseFloat(json[i].Monto)
            b.push(y);
            total+=y;
            $('#tabla tr:last').after('<tr><td>'+json[i].Nombre+'</td><td>'+json[i].TotalVentas+'</td><td>'+json[i].FolioInicial+'</td><td>'+json[i].FolioFinal+'</td><td>$'+json[i].Monto+'</td><td>'+json[i].FechaCorte+'</td></tr>');
        }
        $('#vTotal').text('$'+total);
        return b;
    };
    function graph (json,id) {
        $('#'+id).highcharts({
        //     chart: {
        //     type: 'column'
        // },
        title: {
            text: 'Gráfica de Ventas',
            x: -20 //center
        },
        subtitle: {
            text: 'Uptics Control',
            x: -20
        },
        xAxis: {
            categories: getId(json),
        },
        yAxis: {
            title: {
                text:'Total Vendido $'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }],
            labels: {
                format: '${value}',
                
            },
            title: {
                text: 'Total Vendido $',
                style: {
                    color: "#E8560C"
                }
            },
        },
        tooltip: {
            headerFormat: '<b>Información.<br/>Dia :{point.x}</b><br/>',
            pointFormat: '<span style="color:{series.color}"> Monto:${point.y}</spam>',
            shared: true,
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Información',
            data: getData(json),
        },
        ]
    });
}