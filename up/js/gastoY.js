 var total=0;
 function Proces () {
    var datos=$('#form').serialize();
    jQuery.ajax({
        url: 'gastosY.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
        complete: function(xhr, textStatus) {
        },
        success: function(data, textStatus, xhr) {
            graph(data,'container');
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log('Ocurrio Un error');
        }
    });
}
$('#get').click(function(event) {
    Proces();
});;
$(document).ready(function() {
    Proces();
});
//Obtener X
function getId (json) {
    var D=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return D;
}
//Obtener Datos
function getData(json) {
    var b=[];
    total=0.0;
    var y=0.0;
    var R=[];
    for (var i = 0; i < json.length; i++) {
        var A=json[i].name;
        var f=json[i].data;
        var Obj={
            name:A,
            data:f,
        };
        R.push(Obj);
        Obj.length=0;
    }
    return R;
};
function graph (json,id) {
    $('#'+id).highcharts({
       chart: {
        type: 'column',

    },
    title: {
        text: 'Gráfica de Gastos',
            x: -20 //c<enter
        },
        subtitle: {
            text: 'Uptics Control',
            x: -20
        },
        xAxis: {
            categories: getId(json),
        },
        yAxis: {
            title: {
                text:'Total Vendido $'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:11px">{series.name}: </td>' +
            '<td style="padding:0;font-size:11px"><b>${point.y:.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series:getData(json)
    });
}