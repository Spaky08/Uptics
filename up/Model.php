<?php

/**
*  Modelo 
*/
class Model 
{
	private $host;
	private $user;
	private $pass;
	private $dbname;
	public  $PDO;
	public $IMEI;
	public function init($host='localhost',$user='root',$pass='root',$dbname='uptics_mysql'){
		$this->host 	=$host;
		$this->user 	=$user;
		$this->pass 	=$pass;
		$this->dbname 	=$dbname;
		$this->IMEI='0001';

	}
	public function Conect(){
		try {
			$this->PDO = new PDO("mysql:dbname=$this->dbname;host=$this->host", $this->user, $this->pass);
			$this->PDO->exec("set names utf8");
		}
		catch (PDOException $e) {
			echo 'Falló la conexión: ' . $e->getMessage();
		}

	}
	public function Input($key,$val=0)
	{
		if(isset($_POST[$key])) return $_POST[$key];
		else if(isset($_GET[$key])) return $_GET[$key];
		return $val;
	}
	
	public function __construct()
	{
		$this->init();
		$this->Conect();
	}
	public function getData($sql)
	{
		$k=$this->PDO->query($sql)->fetchAll();
		return json_encode($k);
	}
	public function getVentas()
	{
		$m=$this->Input('mes',date('m'));
		$y=date('Y');
		$sql="select *from caja_cortes where month(FechaCorte)=$m and year(FechaCorte)=$y  order by FechaCorte asc;";
		return $this->getData($sql);
	}
	public function getVentasMes(){
		$y=$this->Input('year',date('Y'));	
		$sql="select date_format(A.FechaCorte,'%Y-%m') as fecha ,sum(A.Monto) as Total,sum(A.TotalVentas) as Ventas from caja_cortes as A where   year(A.FechaCorte)=$y group by fecha;";
		return $this->getData($sql);
	}
	public function getVentaYear(){
		$y=date('Y');
		$yi=$y-2;
		$sql="select sum(A.Monto) as Monto,sum(A.TotalVentas) as Ventas,date_format(A.FechaCorte,'%Y-%m') as fecha , year(A.FechaCorte) as y from caja_cortes as A where   year(A.FechaCorte)>=$yi and year(A.FechaCorte)<=$y  group by fecha; ";
		$k=$this->PDO->query($sql)->fetchAll(PDO::FETCH_OBJ);
		return $k;
	}
	public function getCompras(){
		$m=$this->Input('mes',date('m'));
		$y=date('Y');
		$sql="select A.*, B.NombreComercial as nombreCom from compras as A left join proveedores as B on  A.idProveedores =B.idProveedores where   month(A.FechaCompra)=$m and year(A.FechaCompra)=$y;";
		return $this->getData($sql);
	}
	public function getComprasMes(){
		$y=$this->Input('year',date('Y'));
		$sql=" select sum(A.TotalPiezas) as Piezas, sum(A.costo) as Costo , sum(A.IVA) as Iva ,date_format(A.FechaCompra,'%Y-%m') as Fecha  from Compras as A where year(A.FechaCompra)=$y  group by Fecha;";
		return $this->getData($sql);
	}
	public function getGastos(){
		$y=date('Y');
		$m=$this->Input('mes',date('m'));
		$sql="select A.*,B.Concepto as Cat,sum(A.Monto) as Total  from gastos  as A join gasto_concepto as B on A.idConcepto=B.idConcepto  where   month(A.Fecha)=$m and year(A.Fecha)=$y group by A.idConcepto;";
		return $this->getData($sql);
	}
	public function getGastoMonth(){
		$y=$this->Input('year',date('Y'));
		$sql="select A.*,B.Concepto Cat,month(A.fecha)as mes,sum(A.Monto) Total  from gastos   as A join gasto_concepto as B on A.idConcepto=B.idConcepto  where    year(A.Fecha)=$y group by Cat, mes order by Cat ;";
		$k=$this->PDO->query($sql)->fetchAll(PDO::FETCH_OBJ);
		return $k;
	}
}