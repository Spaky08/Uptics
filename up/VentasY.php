<?php
include('Model.php');
class chart{
	public $keys;
	public $meses;
	public $R;
	function __construct()
	{
		$this->keys = array();
		$this->meses = array();
	}	
	public function algoritmo(&$v,$fecha,$valor,$nombre){
		if($v==NULL){
			$v=$meses = array(0,0,0,0,0,0,0,0,0,0,0,0);
			array_push($this->keys,$nombre);
		}
		else $meses=$v;
		$m=(int)(date('m',strtotime($fecha)))-1;
		$meses["$m"]=$valor;
		$v=$meses;
	}
	public function Construir($datas,$name){
		$d['name']=$name;
		$i=0;
		$d['data']="";
		$r="";
		 foreach ($datas as $k) {
		 	if($i==0)
				$r.=$k;
			else $r.=','.$k;
			$i++;
		 }
		 $d['data']=$r;
		return $d;
	}
	public function addYear($data,$nombre){
		$datas = array();
		foreach ($data as $k) {
			$this->algoritmo($datas[$k->y],$k->fecha,$k->Monto,$k->y);
		}
		$i=0;
		$grafica='';
		$d=array();
		foreach ($datas as $k) {
				array_push($d,$this->Construir($k,$this->keys[$i]) );
				$i++;
		}
		array_push($d,$this->getSpline($datas));

		return $d;
	}
	public function getSpline($datas)
	{
		$A=array();
		$t=0;
		for($i=0;$i<12;$i++){
			$t=0;
			$l=0;
			foreach ($datas as $k) {
				$t+=$k[$i];
				$l++;
			}
			$t/=$l;
			array_push($A, $t);
		}
		$B['spline']=$A;
		return $B;
	}
}
$A=new Model();
$r=$A->getVentaYear();
$B=new chart();
$data=($B->addYear($r,'manuel'));
$r=json_encode($data);
echo $r;



